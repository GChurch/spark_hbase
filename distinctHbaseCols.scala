package com.gabechurch

def distinctHbaseCols(tableName: String,
                                 zkQuorum: String,
                                 targetFamily: String = "MAIN"): Array[String] = {
      @transient val conf = HBaseConfiguration.create()
      conf.set("hbase.zookeeper.quorum", zkQuorum)
      conf.set("hbase.zookeeper.property.clientPort", "2181")
      conf.set("hadoop.security.authentication", "kerberos")
      conf.set("hbase.security.authentication", "kerberos")
      conf.set("hbase.master.kerberos.principal", "YOUR_PRINCIPAL")
      conf.set("hbase.regionserver.kerberos.principal", "YOUR_PRINCIPAL")
      conf.set("hbase.cluster.distributed", "true")
      conf.set("hadoop.ssl.enabled", "true")
      conf.set("hbase.client.scanner.timeout.period", "600000")
      conf.set("hbase.rpc.timeout", "600000")
      @transient val scan = new Scan()
      
      scan.setFilter(new KeyOnlyFilter())
      scan.addFamily(targetFamily.getBytes)

      @transient val stringScan = convertScanToString(scan)
      conf.set(TableInputFormat.INPUT_TABLE, tableName)
      conf.set(TableInputFormat.SCAN, stringScan)
      val hbaseRdd2 = spark.sparkContext.newAPIHadoopRDD(conf,
        classOf[TableInputFormat],
        classOf[ImmutableBytesWritable],
        classOf[Result]
      )
      //efficient method to calculate distinct values. (significantly reduces shuffle vs DF.distinct() esp for single column values)
      val distinctCols2 = hbaseRdd2.mapPartitions(partition => {
        val partitionValues = partition.map(hRow => {
          val result = hRow._2
          val cellResults = result.rawCells()
          val rowResults = cellResults.map(cell => {
            val q = Bytes.copy(cell.getQualifierArray(),
              cell.getQualifierOffset(),
              cell.getQualifierLength())
            Bytes.toString(q)
          })
          rowResults.toSet
        }).fold(Set.empty[String])((acc, MapColumns) => {
          MapColumns.asInstanceOf[Set[String]].union(acc.asInstanceOf[Set[String]])
        })
        partitionValues.toIterator
      }).distinct()
      distinctCols2.collect()
    }